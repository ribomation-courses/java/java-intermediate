package freqs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class WordFreqs_Lambda extends WordFreqsBase {
    public static void main(String[] args) {
        WordFreqs_Lambda app = new WordFreqs_Lambda();
        app.run();
    }

    void run() {
        Map<String, Integer>             map  = load(NAME);
        List<Map.Entry<String, Integer>> list = sort(new ArrayList<>(map.entrySet()));
        System.out.printf("The %d most frequent words in %s:%n%s%n",
                maxWordCount, NAME, String.join("\n", transform(take(list, maxWordCount))));
    }

    List<Map.Entry<String, Integer>> sort(List<Map.Entry<String, Integer>> words) {
        Collections.sort(words, (left, right) -> Integer.compare(right.getValue(), left.getValue()));
        return words;
    }

    List<Map.Entry<String, Integer>> take(List<Map.Entry<String, Integer>> words, int n) {
        return new ArrayList<>(words.subList(0, n));
    }

    List<String> transform(List<Map.Entry<String, Integer>> words) {
        List<String> result = new ArrayList<>();
        for (Map.Entry<String, Integer> e : words) {
            result.add(String.format("%-10s: %d", e.getKey(), e.getValue()));
        }
        return result;
    }
}
