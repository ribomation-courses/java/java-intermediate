package freqs;

import static java.lang.Integer.compare;

public class WordFreqs_Streams extends WordFreqsBase {
    public static void main(String[] args) {
        WordFreqs_Streams app = new WordFreqs_Streams();
        app.run();
    }

    void run() {
        String result = load(NAME)
                .entrySet().stream()
                .sorted((left, right) -> compare(right.getValue(), left.getValue()))
                .limit(maxWordCount)
                .map((e) -> String.format("%-10s: %d", e.getKey(), e.getValue()))
                .reduce("", (txt, wf) -> txt + wf + "\n")
                ;
        System.out.printf("The %d most frequent words in %s:%n%s",
                maxWordCount, NAME, result);
    }
}
