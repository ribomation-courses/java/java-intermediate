package freqs;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class WordFreqsBase {
    protected String NAME         = "/The-Three-Musketeers.txt";
    protected int    maxWordCount = 50;
    protected int    minWordSize  = 4;

    Map<String, Integer> load(String name) {
        return load(getClass().getResourceAsStream(name));
    }

    Map<String, Integer> load(InputStream is) {
        Map<String, Integer> wordFreqs = new HashMap<>();
        Scanner              scanner   = new Scanner(is).useDelimiter("[^a-zA-Z’]+");
        while (scanner.hasNext()) {
            String word = scanner.next();
            if (word.length() < minWordSize) continue;
            insert(wordFreqs, word.toLowerCase());
        }
        return wordFreqs;
    }

    void insert(Map<String, Integer> m, String word) {
        m.put(word, m.getOrDefault(word, 0) + 1);
    }
}
