package change.me;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

public class GenerateDDL {
    public String generate(String cls) throws ClassNotFoundException {
        return createDDL(Class.forName(cls));
    }
    
    public String generate(Class cls) {
        Annotation table = cls.getAnnotation(Table.class);
        if (table == null) throw new RuntimeException("Missing @Table");

        StringWriter buf = new StringWriter();
        PrintWriter out = new PrintWriter(buf);
        
        String tableName = cls.getSimpleName();
        out.printf("CREATE TABLE %s (%n", tableName);

        for (Field field : cls.getDeclaredFields()) {
            Column column = field.getAnnotation(Column.class);
            if (column == null) continue;

            String  colName    = field.getName();
            String  sqlType    = column.value();
            boolean primaryKey = (field.getAnnotation(PrimaryKey.class) != null);

            out.printf("  %s\t%s%s,%n", colName, sqlType, primaryKey ? " PRIMARY KEY" : "");
        }
        int lastComma = buf.getBuffer().lastIndexOf(",");
        if (lastComma > 0) buf.getBuffer().deleteCharAt(lastComma);
        out.printf(");");
        out.close();

        return buf.toString();
    }
}
