package io;

import io.LineSlurper;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.zip.GZIPInputStream;

public class WeatherDataSlurper {
    public static void main(String[] args) throws IOException {
        WeatherDataSlurper app = new WeatherDataSlurper();
        app.parseArgs(args);
        app.run();
    }

    private String           url    = "http://www1.ncdc.noaa.gov/pub/data/gsod/2016/718280-99999-2016.op.gz";
    private SimpleDateFormat fmtIn  = new SimpleDateFormat("yyyyMMdd");
    private SimpleDateFormat fmtOut = new SimpleDateFormat("yyyy MMM dd");

    public void parseArgs(String[] args) {
        if (args.length > 0) {
            url = args[0];
        }
    }

    public void run() throws IOException {
        LineSlurper slurper = new LineSlurper(new GZIPInputStream(new URL(url).openStream()));
        for (String line : slurper) {
            String[] fields = line.split("\\s+");
            try {
                String date        = toDate(fields[2]);
                double temperature = toCelsius(Double.parseDouble(fields[3]));
                System.out.printf(Locale.US, "%12s: %6.2f%n", date, temperature);
            } catch (Exception ignore) {
            }
        }
    }

    private String toDate(String d) throws ParseException {
        return fmtOut.format(fmtIn.parse(d));
    }

    private double toCelsius(double fahrenheit) {
        return (fahrenheit - 32) / 1.8;
    }
}
