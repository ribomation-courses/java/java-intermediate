package io;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Iterator;

public class LineSlurper implements Iterable<String> {
    private BufferedReader in;

    public LineSlurper(InputStream is) {
        this.in = new BufferedReader(new InputStreamReader(is));
    }

    public LineSlurper(Reader r) {
        this.in = new BufferedReader(r);
    }

    public Iterator<String> iterator() {
        return new Iterator<String>() {
            private String currentLine;

            {
                currentLine = readLine();
            }

            public boolean hasNext() {
                return currentLine != null;
            }

            public String next() {
                String value = currentLine;
                currentLine = readLine();
                if (currentLine == null) close();
                return value;
            }

            public void remove() {
                throw new UnsupportedOperationException("remove()");
            }

            private String readLine() {
                try {
                    return in.readLine();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }

            private void close() {
                try {
                    in.close();
                } catch (Exception ignore) { }
            }
        };
    }
}
