package io;

import io.LineSlurper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class LineSlurperApp {
    public static void main(String[] args) throws FileNotFoundException {
        if (args.length > 0) {
            for (String arg : args) slurp(arg);
        } else {
            File f = new File("./src/java/" + LineSlurper.class.getName().replace('.', File.separatorChar) + ".java");
            if (!f.canRead()) {
                throw new IllegalArgumentException("Cannot find/read file: " + f);
            }
            slurp(f.getAbsolutePath());
        }
    }

    static int lineno = 1;
    
    private static void slurp(String file) throws FileNotFoundException {
        for (String line : new LineSlurper(new FileReader(file))) {
            System.out.printf("%3d) %s%n", lineno++, line);
        }
    }
}
