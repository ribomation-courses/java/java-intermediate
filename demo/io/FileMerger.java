package io;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class FileMerger {
    public static void main(String[] args) throws Exception {
        FileMerger app = new FileMerger();
        app.load(args);
        app.dump();
    }

    private List<String> lines = new ArrayList<String>();

    public void load(String[] args) throws IOException {
        if (args.length==0)
            readStream(System.in);
        else {
            for (int i=0; i<args.length; ++i)
                if (args[i].startsWith("http:"))
                    readStream(new URL(args[i]).openStream());
                else
                    readStream(new FileInputStream(args[i]));
        }
    }

    public void dump() {
        int lineno = 1;
        for (String line : lines) System.out.printf("%4d) %s%n", lineno++, line);
    }

    public void readStream(InputStream is) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(is));
        String line;
        while ((line = in.readLine()) != null) lines.add(line);
        in.close();
    }
}
