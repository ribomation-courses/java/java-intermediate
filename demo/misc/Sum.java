package misc;

import java.util.function.UnaryOperator;

public class Sum {
    public static void main(String[] args) {
        UnaryOperator<Integer> sum = n -> n * (n + 1) / 2;
        System.out.printf("SUM(1..%d) = %d%n", 10, sum.apply(10));
        System.out.printf("SUM(1..%d) = %d%n", 100, sum.apply(100));
        System.out.printf("SUM(1..%d) = %d%n", 1000, sum.apply(1000));
    }
}
