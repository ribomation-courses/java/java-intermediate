package misc;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Count {
    public static void main(String[] args) throws IOException {
        Path   path   = Paths.get("./src/resources/The-Three-Musketeers.txt");
        String phrase = "cardinal";
        long count = Files.lines(path)
                .filter((line) -> line.toLowerCase().contains(phrase))
                .count();
        System.out.printf("%d lines with phrase '%s'%n", count, phrase);
    }
    
}
