package ribomation.java_intermediate.persons;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Person {
    private String  name;
    private boolean female;
    private int     age;
    private int     postCode;

    public static Person fromCSV(String csv) { return fromCSV(csv, ","); }

    public static Person fromCSV(String csv, String delim) {
        Person   p  = new Person();
        String[] f  = csv.split(delim);
        int      ix = 0;
        p.name     = f[ix++];
        p.female   = f[ix++].equals("F");
        p.age      = Integer.parseInt(f[ix++]);
        p.postCode = Integer.parseInt(f[ix]);
        return p;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", female=" + (female ? "Yes" : "No") +
                ", age=" + age +
                ", postCode=" + Stream.of(postCode)
                                      .map(p -> Integer.toString(p))
                                      .map(s -> String.format("%s %s",s.substring(0,3), s.substring(3)))
                                      .collect(Collectors.joining())
                + '}';
    }

    public String  getName()     { return name; }
    public boolean isFemale()    { return female; }
    public int     getAge()      { return age; }
    public int     getPostCode() { return postCode; }
}
