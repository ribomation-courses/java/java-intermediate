package ribomation.java_intermediate.persons_alt_2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class PersonsStream {
    public static void main(String[] args) throws Exception {
        PersonsStream app = new PersonsStream();
        app.run();
    }

    void run() throws IOException {
        /*List<Person> people = */Files
                .lines(Paths.get("./data/persons-data.alt-many.csv"))
                .skip(1)
                .map(Person::fromCSV)
                .filter(p -> 30 <= p.getAge() && p.getAge() <= 40)
                .filter(p -> p.getPostCode() < 20_000)
                .filter(Person::isFemale)
                .forEach(System.out::println);
                //.peek(System.out::println)
                //.collect(Collectors.toList());

//        people.forEach(System.out::println);
    }
}
