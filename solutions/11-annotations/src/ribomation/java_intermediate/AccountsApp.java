package ribomation.java_intermediate;

import ribomation.java_intermediate.ddl.GenerateDDL;

public class AccountsApp {
    public static void main(String[] args) throws Exception {
        AccountsApp app = new AccountsApp();
        app.run("ribomation.java_intermediate.domain.Account");
        app.run("ribomation.java_intermediate.domain.Invoice");
    }

    void run(String clsName) throws Exception {
        System.out.printf("--- DDL SQL ---\n%s%n",
                GenerateDDL.instance.generate(clsName));
    }

}
