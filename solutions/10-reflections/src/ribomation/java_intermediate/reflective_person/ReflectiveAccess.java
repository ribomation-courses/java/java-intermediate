package ribomation.java_intermediate.reflective_person;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectiveAccess {
    public static void main(String[] args) throws Exception {
        ReflectiveAccess app = new ReflectiveAccess();
        app.run("ribomation.java_intermediate.reflective_person.domain.Person");
    }

    void run(String className) throws Exception {
        Class<?> cls = Class.forName(className);

        Constructor<?> constructor = cls.getConstructor(String.class, String.class);
        Object obj = constructor.newInstance("Anna", "Conda");
        System.out.printf("obj: %s%n", obj);

        Method setAge = cls.getDeclaredMethod("setAge", int.class);
        setAge.invoke(obj, 42);
        System.out.printf("obj: %s%n", obj);

        Field female = cls.getDeclaredField("female");
        female.setAccessible(true);
        female.set(obj, true);
        System.out.printf("obj: %s%n", obj);
    }
}
