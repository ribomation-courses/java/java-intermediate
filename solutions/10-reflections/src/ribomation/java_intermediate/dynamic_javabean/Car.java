package ribomation.java_intermediate.dynamic_javabean;

public interface Car {
    String  getLicense();
    int     getSpeed();
    boolean isTurbo();
    void    move(int speed);
}
