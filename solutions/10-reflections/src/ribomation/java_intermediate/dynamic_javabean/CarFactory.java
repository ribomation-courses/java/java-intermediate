package ribomation.java_intermediate.dynamic_javabean;

import java.io.Serializable;
import java.lang.reflect.Proxy;

public class CarFactory {
    public static final CarFactory instance = new CarFactory();
    private static final ClassLoader loader = CarFactory.class.getClassLoader();
    private static final Class[] interfaces = {Car.class, Serializable.class};

    private CarFactory() {
    }

    public Car create(String license, boolean turbo) {
        return (Car) Proxy.newProxyInstance(loader, interfaces, new CarHandler(license, turbo));
    }
}
