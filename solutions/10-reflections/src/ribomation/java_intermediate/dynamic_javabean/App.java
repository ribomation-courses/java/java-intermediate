package ribomation.java_intermediate.dynamic_javabean;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.run();
    }

    void run() {
        Car car1 = CarFactory.instance.create("ABC123", true);
        System.out.printf("car-1: %s%n", car1);
        System.out.printf("car-1.lic: %s%n", car1.getLicense());
        System.out.printf("car-1.speed: %s%n", car1.getSpeed());
        System.out.printf("car-1.turbo: %s%n", car1.isTurbo());

        car1.move(110);
        System.out.printf("car-1: %s%n", car1);

        car1.move(0);
        System.out.printf("car-1: %s%n", car1);
    }

}
