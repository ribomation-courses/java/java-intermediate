package ribomation.java_intermediate.dynamic_javabean;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.TreeMap;

public class CarHandler implements InvocationHandler {
    private Map<String, Object> properties = new TreeMap<>();

    public CarHandler(String license, boolean turbo) {
        properties.put("license", license);
        properties.put("turbo", turbo);
        properties.put("speed", 0);
    }

    @Override
    public Object invoke(Object obj, Method method, Object[] args) throws Throwable {
        switch (method.getName()) {
            case "getLicense":
                return properties.getOrDefault("license", "");

            case "getSpeed":
                return properties.getOrDefault("speed", 0);

            case "isTurbo":
                return properties.getOrDefault("turbo", false);

            case "move":
                Integer speed = (Integer) args[0];
                properties.put("speed", speed);
                break;

            case "toString":
                return properties.toString();
        }

        return null;
    }
}
