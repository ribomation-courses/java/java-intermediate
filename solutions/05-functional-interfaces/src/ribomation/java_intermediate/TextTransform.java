package ribomation.java_intermediate;

interface TextTransform {
    String transform(String txt);
}
