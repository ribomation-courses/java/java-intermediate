Instructions
====

Before compiling and running the tests, you need to add the JAR files in
`./lib/*.jar` to the CLASSPATH.

Gradle
----

This project also has a Gradle build file plus its wrapper.

Use the provided wrapper script and you don't have to install Gradle.
###Windows
    C:\> gradlew.bat build
###Linux/BASH
    $ ./gradlew build

The build task is similar to `mvn package` as it runs the test suite as well.

You can find a generated test report in `./build/reports/tests/index.html`

###Available Tasks
Run the command below to list all available tasks

    ./gradlew tasks


