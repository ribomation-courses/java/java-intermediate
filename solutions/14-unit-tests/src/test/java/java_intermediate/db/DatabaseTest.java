package java_intermediate.db;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.endsWith;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DatabaseTest {
    @Mock
    Database db;
    PersonDAO target;

    @Before public void init() {
        target = new PersonDAO(db);
    }

    @Test 
    public void there_should_be_3_persons() {
        when(db.count("persons")).thenReturn(3);

        assertThat(target.numPersons(), is(3));

        verify(db, times(1)).count("persons");
        verify(db, never()).firstRow(anyString());
    }

    @Test
    public void should_find_person_in_db() {
        Map<String, String> anna = new HashMap<String, String>();
        anna.put("name", "Anna Conda");
        anna.put("age", "42");
        when(db.firstRow("SELECT * FROM persons WHERE name = 'anna'")).thenReturn(anna);

        Person p = target.find("anna");
        assertThat(p, notNullValue());
        assertThat(p.getName(), endsWith("onda"));
        assertThat(p.getAge(), is(42));

        verify(db, times(1)).firstRow(anyString());
        verify(db, times(0)).count("persons");
    }

    @Test
    public void should_return_null_if_not_found() {
        Map<String, String> anna = new HashMap<String, String>();
        anna.put("name", "Anna Conda");
        anna.put("age", "42");
        
        when(db.firstRow("SELECT * FROM persons WHERE name = 'anna'")).thenReturn(anna);
        when(db.firstRow("SELECT * FROM persons WHERE name = 'boris'")).thenReturn(null);

        try {
            Person p = target.find("boris");
            assertThat(p, nullValue());
        } catch (Exception e) {
            assertThat(e.getMessage(), is("No such person name: boris"));
        }

        verify(db, times(1)).firstRow(anyString());
    }


    @Test
    public void insert_should_increment_person_count() {
        when(db.count("persons"))
                .thenReturn(3)
                .thenReturn(4);
        
        assertThat(target.numPersons(), is(3));
        
        Person nisse = new Person("Nisse", 45);
        Person result = target.insert(nisse);
        
        assertThat(result, notNullValue());
        assertThat(target.numPersons(), is(4));

        verify(db, times(2)).count("persons");
        verify(db, times(1)).insert("insert into persons (name,age) values ('Nisse',45)");
    }
    
}

