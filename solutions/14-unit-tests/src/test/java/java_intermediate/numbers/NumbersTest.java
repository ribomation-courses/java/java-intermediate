package java_intermediate.numbers;

import org.junit.*;

import static org.junit.Assert.*;

public class NumbersTest {
    private Numbers target;

    @BeforeClass
    public static void beforeAll() {
        System.out.printf("Starting all tests%n%n");
    }

    @Before
    public void beforeTest() throws Exception {
        System.out.printf("*** Running a single test%n");
        target = new Numbers();
    }

    @After
    public void afterTest() throws Exception {
        System.out.printf("    Done with a single test%n");
        target = new Numbers();
    }

    @AfterClass
    public static void afterAll() {
        System.out.printf("Done with all tests%n");
    }


    @Test
    public void positive_args_should_work() throws Exception {
        assertEquals(1, target.fibonacci(2));
        assertEquals(55, target.fibonacci(10));
    }

    @Test(expected = IllegalArgumentException.class)
    public void negative_argument_should_throw_exception() throws Exception {
        target.fibonacci(-1);
    }

    @Test
    public void square_few_ints_should_work() throws Exception {
        int[] input    = {1, 2, 3, 4, 5};
        int[] actual   = target.square(input);
        int[] expected = {1, 4, 9, 16, 25};
        assertArrayEquals(expected, actual);
    }

    @Ignore("not yet ready for testing")
    @Test
    public void not_ready_for_testing() throws Exception {
        fail("Should not see this");
    }

    @Ignore
    @Test
    public void failing_test() throws Exception {
        assertEquals(57, target.fibonacci(10));
    }

    @Ignore
    @Test(timeout = 500)
    public void too_long_time() {
        target.fibonacci(42);
    }

}

