package java_intermediate.numbers;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Properties;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class BigNumbersTest {
    private BigNumbers target;
    private Properties bignums;

    @Before
    public void init() throws IOException {
        target = new BigNumbers();
        bignums = new Properties();
        bignums.load(getClass().getResourceAsStream("/bignums.properties"));
    }

    @Test
    public void simple_factorial_should_pass() throws Exception {
        assertThat(target.factorial(1), is(BigInteger.valueOf(1)));
        assertThat(target.factorial(5), is(BigInteger.valueOf(120)));
    }

    @Test
    public void simple_fibonacci_should_pass() {
        assertThat(target.fibonacci(1), is(BigInteger.valueOf(1)));
        assertThat(target.fibonacci(2), is(BigInteger.valueOf(1)));
        assertThat(target.fibonacci(10), is(BigInteger.valueOf(55)));
    }

    @Test
    public void factorial_10_should_pass() throws Exception {
        assertThat(target.factorial(10), is(BigInteger.valueOf(3628800)));
    }

    @Test
    public void factorial_100_should_pass() throws Exception {
        assertThat(target.factorial(100), is(new BigInteger(bignums.getProperty("factorial.100"))));
    }

    @Test
    public void fibonacci_100_should_pass() {
        assertThat(target.fibonacci(100), is(new BigInteger(bignums.getProperty("fibonacci.100"))));
    }

    @Test(expected = IllegalArgumentException.class)
    public void negative_arg_to_factorial_should_throw() {
        target.factorial(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void negative_arg_to_fibonacci_should_throw() {
        target.fibonacci(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void zero_arg_to_fibonacci_should_throw() {
        target.fibonacci(0);
    }

}
