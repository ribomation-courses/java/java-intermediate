package java_intermediate.numbers;

import java.math.BigInteger;

public class BigNumbers {

    public BigInteger factorial(int n) {
        if (n < 1) {
            throw new IllegalArgumentException("Negative or zero");
        }

        if (n == 1) {
            return BigInteger.ONE;
        }

        BigInteger result = BigInteger.ONE;
        for (int k = 2; k <= n; ++k) {
            result = result.multiply(BigInteger.valueOf(k)); // result += k
        }

        return result;
    }


    public BigInteger fibonacci(int n) {
        if (n < 1) {
            throw new IllegalArgumentException("Negative or zero");
        }

        if (n == 1 || n == 2) {
            return BigInteger.ONE;
        }

        BigInteger f0 = BigInteger.ZERO;
        BigInteger f1 = BigInteger.ONE;
        for (int k = 2; k <= n; ++k) {
            BigInteger f = f1.add(f0);
            f0 = f1;
            f1 = f;
        }
        
        return f1;
    }

}
