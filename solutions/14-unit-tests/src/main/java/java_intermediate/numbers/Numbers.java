package java_intermediate.numbers;

public class Numbers {

    public long fibonacci(int n) {
        if (n < 1) throw new IllegalArgumentException("Non-positive number " + n);
        
        return (n == 1 || n == 2) ? 1 : fibonacci(n - 1) + fibonacci(n - 2);
    }

    public int[] square(int[] arr) {
        for (int k = 0; k < arr.length; k++) arr[k] *= arr[k];
        return arr;
    }
    
}
