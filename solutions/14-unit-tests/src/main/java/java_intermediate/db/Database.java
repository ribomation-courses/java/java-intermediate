package java_intermediate.db;

import java.util.Map;

public interface Database {
    int count(String tblName);

    Map<String, String> firstRow(String sql);

    void insert(String sql);
}

