package java_intermediate.db;

import java.util.Map;

public class PersonDAO {
    Database db;

    public PersonDAO(Database db) {
        this.db = db;
    }

    int numPersons() {
        return db.count("persons");
    }

    Person find(String name) {
        Map<String, String> row = db.firstRow("SELECT * FROM persons WHERE name = '" + name + "'");
        if (row == null) throw new IllegalArgumentException("No such person name: " + name);

        return new Person(row.get("name"), Integer.parseInt(row.get("age")));
    }

    Person insert(Person p) {
        db.insert(String.format("insert into %s (name,age) values ('%s',%d)",
                "persons", p.getName(), p.getAge()));
        return p;
    }
}
