package ribomation.java_intermediate.words;

import java.io.InputStream;
import java.util.*;

/**
 * Reads a file and prints the N most frequent words.
 *
 * @author Jens Riboe, jens.riboe@ribomation.se
 * @date 2017-01-18
 */
public class WordFreqs {
    public static void main(String[] args) {
        WordFreqs app = new WordFreqs();
        app.run(25, 5);
    }

    void run(int maxWords, int minWordSize) {
        Scanner              input = openInput();
        Map<String, Integer> freq  = load(input, minWordSize);
        freq = sort(freq);
        freq = truncate(freq, maxWords);
        print(freq);
    }

    Scanner openInput() {
        String      path = "/The-Three-Musketeers.txt";
        InputStream is   = getClass().getResourceAsStream(path);
        if (is == null) {
            throw new RuntimeException("cannot open " + path);
        }
        return new Scanner(is).useDelimiter("[^a-zA-Z’]+");
    }

    Map<String, Integer> load(Scanner input, int minWordSize) {
        Map<String, Integer> freq = new TreeMap<>();
        while (input.hasNext()) {
            String word = input.next();
            if (word.length() < minWordSize) continue;
            word = word.toLowerCase();
            freq.put(word, 1 + freq.getOrDefault(word, 0));
        }
        return freq;
    }

    Map<String, Integer> sort(Map<String, Integer> freq) {
        List<Map.Entry<String, Integer>> list = new ArrayList<>(freq.entrySet());
        list.sort((left, right) -> Integer.compare(right.getValue(), left.getValue()));

        Map<String, Integer> result = new LinkedHashMap<>();
        for (Map.Entry<String, Integer> e : list) {
            result.put(e.getKey(), e.getValue());
        }
        return result;
    }

    Map<String, Integer> truncate(Map<String, Integer> data, int maxElements) {
        Map<String, Integer> result = new LinkedHashMap<>();
        for (Map.Entry<String, Integer> e : data.entrySet()) {
            result.put(e.getKey(), e.getValue());
            if (--maxElements <= 0) return result;
        }
        return result;
    }

    void print(Map<String, Integer> wordFreqs) {
        wordFreqs.forEach((word, freq) -> System.out.printf("%-15s: %3d%n", word, freq));
    }
}
