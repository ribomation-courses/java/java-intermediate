package ribomation.java_intermediate.fibonacci;

/**
 * Test program for FibonacciRange
 *
 * @author Jens Riboe, jens.riboe@ribomation.se
 * @date 2017-01-18
 */
public class FibonacciApp {
    public static void main(String[] args) {
        int n = 1;
        int numFibs = args.length == 0 ? 42 : Integer.parseInt(args[0]);

        for (Long f : new FibonacciRange(numFibs)) {
            System.out.printf("fibonacci(%d) = %d%n", n++, f);
        }
    }
}
