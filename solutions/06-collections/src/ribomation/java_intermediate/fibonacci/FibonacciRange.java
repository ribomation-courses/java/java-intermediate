package ribomation.java_intermediate.fibonacci;

import java.util.Iterator;

public class FibonacciRange implements Iterable<Long> {
    private int maxCount;

    public FibonacciRange(int maxCount) {
        this.maxCount = maxCount;
    }

    @Override
    public Iterator<Long> iterator() {
        return new Iterator<Long>() {
            private long f1, f2;
            private int count;

            {
                f1 = 1; f2 = 0;
                count = maxCount;
            }
            
            @Override
            public boolean hasNext() { return count > 0; }

            @Override
            public Long next() {
                long value = f1 + f2;
                f1 = f2;
                f2 = value;
                --count;
                return value;
            }
        };
    }
}
