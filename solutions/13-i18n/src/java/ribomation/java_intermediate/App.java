package ribomation.java_intermediate;

import javax.swing.*;
import java.awt.*;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

public class App extends JFrame {
    public static void main(String[] args) {
        App app = new App();
        app.setupUI();
        app.run();
    }

    List<AbstractButton> buttons = new ArrayList<>();
    List<JLabel> labels = new ArrayList<>();

    Locale[] supportedLocales = {
            Locale.forLanguageTag("sv-SE"),
            Locale.US,
            Locale.GERMAN,
    };


    void setupUI() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setTitle("i18n");
        this.setLayout(new BorderLayout());

        this.setJMenuBar(createMenuBar());
        this.add(createToolBar(), BorderLayout.NORTH);
        this.add(createContent(), BorderLayout.CENTER);
    }

    void run() {
        updateUI();
        this.setSize(600, 400);
        this.setVisible(true);
    }

    JPanel createContent() {
        JLabel lbl = new JLabel();
        lbl.setName("message.greeting");
        labels.add(lbl);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.CENTER));
        p.add(lbl);

        return p;
    }

     JPanel createToolBar() {
        JButton msg = new JButton("Message");
        msg.setName("toolbar.msg");
        buttons.add(msg);
        msg.addActionListener((e) -> {
            JOptionPane.showMessageDialog(null,
                    getString("message.greeting"),
                    "i18n",
                    JOptionPane.INFORMATION_MESSAGE);
        });

        JButton update = new JButton("Update UI");
        update.setName("action.update");
        buttons.add(update);
        update.addActionListener((e) -> updateUI());

        JButton quit = new JButton("Quit");
        quit.setName("action.quit");
        buttons.add(quit);
        quit.addActionListener((e) -> System.exit(0));

        JPanel p = new JPanel(new FlowLayout());
        p.add(msg);
        p.add(update);
        p.add(quit);

        return p;
    }

    JMenuBar createMenuBar() {
        JMenuBar mb = new JMenuBar();
        mb.add(createFileMenu());
        mb.add(createI18nMenu());
        return mb;
    }

    JMenu createFileMenu() {
        JMenuItem quit = new JMenuItem("Quit");
        quit.setName("action.quit");
        quit.addActionListener((e) -> {
            System.exit(0);
        });
        buttons.add(quit);

        JMenuItem update = new JMenuItem("Update UI");
        update.setName("action.update");
        update.addActionListener((e) -> {
            updateUI();
        });
        buttons.add(update);

        JMenu m = new JMenu("File");
        m.setName("menu.file");
        buttons.add(m);
        m.add(update);
        m.add(quit);
        return m;
    }

    JMenu createI18nMenu() {
        JMenu m = new JMenu("i18n");
        m.setName("menu.i18n");
        buttons.add(m);

        for (Locale loc : supportedLocales) {
            JMenuItem mi = new JMenuItem(loc.getDisplayName());
            m.add(mi);
            mi.addActionListener((e) -> {
                Locale.setDefault(loc);
                updateUI();
            });
        }

        return m;
    }

    void updateUI() {
        ResourceBundle msgs = ResourceBundle.getBundle("messages");
        for (AbstractButton e : buttons) {
            String key = e.getName();
            if (key == null) key = "XXX";
            e.setText(msgs.containsKey(key) ? msgs.getString(key) : key);
        }
        for (JLabel l : labels) {
            String key = l.getName();
            if (key == null) key = "XXX";
            l.setText(msgs.containsKey(key) ? msgs.getString(key) : key);
        }
    }

    String getString(String key) {
        ResourceBundle msgs = ResourceBundle.getBundle("messages");
        return String.format("%s (%s)",
                msgs.getString(key),
                DateFormat.getDateInstance().format(new Date())
        );
    }

}
