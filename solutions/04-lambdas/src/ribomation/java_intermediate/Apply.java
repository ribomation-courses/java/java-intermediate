package ribomation.java_intermediate;

public interface Apply<T> {
    T transform(T x);
}
