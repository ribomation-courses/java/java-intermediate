package ribomation.java_intermediate;

public class Pair<T> {
    public final T first;
    public final T last;

    public Pair(T first, T last) {
        this.first = first;
        this.last  = last;
    }
}
