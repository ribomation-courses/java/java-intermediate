package ribomation.java_intermediate;

import java.time.LocalDate;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Comparison of date-time computation in classic and modern java
 *
 * @author Jens Riboe, jens.riboe@ribomation.se
 * @date 2017-01-19
 */
public class DateTimeApp {
    public static void main(String[] args) {
        DateTimeApp app = new DateTimeApp();
        app.classic();
        app.modern();
    }

    Date[] dates = {
            new Date(),
            new GregorianCalendar(2015,1,10).getTime(),
            new GregorianCalendar(2016,1,20).getTime()
    };

    void classic() {
        System.out.println("--- Classic Java ---");
        computeClassic(dates[0]);
        computeClassic(dates[1]);
        computeClassic(dates[2]);
    }

    void modern() {
        System.out.println("--- Modern Java ---");
        computeModern(dates[0]);
        computeModern(dates[1]);
        computeModern(dates[2]);
    }

    private void computeClassic(Date date) {
        Date lastDay = DateTimeUtilClassic.lastDayOfMonth(date);
        System.out.printf("today: %tF -> last-day: %tF%n", date, lastDay);
    }

    private void computeModern(Date date) {
        Pair<LocalDate> result = DateTimeUtilModern.firstAndLastDayOfMonth(date);
        System.out.printf("today: %tF -> first/last: %tF / %tF%n", date, result.first, result.last);
    }
}
