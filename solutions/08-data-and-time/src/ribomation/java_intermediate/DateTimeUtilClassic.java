package ribomation.java_intermediate;

import java.util.Calendar;
import java.util.Date;

/**
 * Date-Time util funcs, based on classic java
 *
 * @author Jens Riboe, jens.riboe@ribomation.se
 * @date 2017-01-19
 */
public class DateTimeUtilClassic {

    public static Date lastDayOfMonth(Date dayInMonth) {
        Calendar c = Calendar.getInstance();
        c.setTime(dayInMonth);

        int lastDay = c.getActualMaximum(Calendar.DAY_OF_MONTH);
        c.set(Calendar.DAY_OF_MONTH, lastDay);

        return c.getTime();
    }

}
