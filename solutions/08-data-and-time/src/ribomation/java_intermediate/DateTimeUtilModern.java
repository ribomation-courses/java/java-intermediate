package ribomation.java_intermediate;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjusters;
import java.util.AbstractMap;
import java.util.Date;
import java.util.Map;

/**
 * Date-Time util funcs, based on modern java
 *
 * @author Jens Riboe, jens.riboe@ribomation.se
 * @date 2017-01-19
 */
public class DateTimeUtilModern {

    public static Pair<LocalDate> firstAndLastDayOfMonth(Date date) {
        //see: http://stackoverflow.com/questions/21242110/convert-java-util-date-to-java-time-localdate
        LocalDate current = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate first   = current.with(TemporalAdjusters.firstDayOfMonth());
        LocalDate last    = current.with(TemporalAdjusters.lastDayOfMonth());
        return new Pair<>(first, last);
    }

}
