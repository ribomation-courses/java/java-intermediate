package ribomation.java_intermediate;

public class SimpleQueue<ElemType> {
    private final ElemType[] storage;
    private       int        size     = 0;
    private       int        putIndex = 0;
    private       int        getIndex = 0;

    @SuppressWarnings("unchecked")
    public SimpleQueue(int capacity) {
        storage = (ElemType[]) new Object[capacity];
    }
    public int capacity() {
        return storage.length;
    }

    public int size() {
        return this.size;
    }

    public boolean empty() {
        return size() == 0;
    }

    public boolean full() {
        return size() == capacity();
    }

    public void put(ElemType x) {
        if (full()) {
            throw new IllegalArgumentException("queue full");
        }
        storage[putIndex] = x;
        putIndex = (putIndex + 1) % capacity();
        ++size;
    }

    public ElemType get() {
        if (empty()) {
            throw new IllegalArgumentException("queue empty");
        }
        try {
            return storage[getIndex];
        } finally {
            getIndex = (getIndex + 1) % capacity();
            --size;
        }
    }

}
