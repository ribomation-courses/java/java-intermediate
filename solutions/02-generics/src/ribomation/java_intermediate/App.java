package ribomation.java_intermediate;

import java.time.LocalDate;
import java.time.temporal.TemporalUnit;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.use_ints();
        app.use_strings();
        app.use_dates();
    }

    void use_ints() {
        SimpleQueue<Integer> q = new SimpleQueue<>(10);
        int number = 1;
        while (!q.full()) {
            q.put(number++);
        }
        System.out.print("Integers: ");
        while (!q.empty()) {
            System.out.printf("%d ", q.get());
        }
        System.out.println();
    }

    void use_strings() {
        SimpleQueue<String> q = new SimpleQueue<>(10);
        String value = "str-";
        int number = 1;
        while (!q.full()) {
            q.put((value + number++).toUpperCase());
        }
        System.out.print("Strings: ");
        while (!q.empty()) {
            System.out.printf("%s ", q.get());
        }
        System.out.println();
    }

    void use_dates() {
        SimpleQueue<LocalDate> q = new SimpleQueue<>(10);
        LocalDate date = LocalDate.now();
        int number = 0;
        while (!q.full()) {
            q.put(date.plusDays(number++));
        }
        System.out.print("Dates: ");
        while (!q.empty()) {
            System.out.printf("%tF ", q.get());
        }
        System.out.println();
    }

}
