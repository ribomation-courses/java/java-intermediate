package ribomation.java_intermediate;

import java.util.Arrays;
import java.util.Comparator;

public class Sorting {
    public static void main(String[] args) {
        Sorting app = new Sorting();
        app.run();
    }

     void run() {
         String[] words = create();
         System.out.printf("Initial: %s%n", Arrays.toString(words));

         words = sortLexicalDesc(words);
         System.out.printf("Lexical: %s%n", Arrays.toString(words));

         words = sortSizeDesc(words);
         System.out.printf("Size   : %s%n", Arrays.toString(words));
     }

    String[] create() {
        return new String[]{
                "Java", "is", "a", "cool", "language", "!!!"
        };
    }

    String[] sortLexicalDesc(String[] words) {
        Arrays.sort(words, new Comparator<String>() {
            @Override
            public int compare(String left, String right) {
                return right.compareTo(left);
            }
        });
        return words;
    }

    String[] sortSizeDesc(String[] words) {
        Arrays.sort(words, new Comparator<String>() {
            @Override
            public int compare(String left, String right) {
                return Integer.compare(right.length(), left.length());
            }
        });
        return words;
    }

}
