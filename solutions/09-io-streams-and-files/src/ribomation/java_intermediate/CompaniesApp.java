package ribomation.java_intermediate;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Loads a bunch of companies read from a CSV file,
 * stores a list of the all into a serialized files,
 * and then restores the list and prints them out.
 *
 * @author Jens Riboe, jens.riboe@ribomation.se
 * @date 2017-01-19
 */
public class CompaniesApp {
    public static void main(String[] args) throws Exception {
        CompaniesApp app = new CompaniesApp();
        app.run("./data/company-data.csv");
    }

    @SuppressWarnings("unchecked")
    void run(String csvFile) throws IOException, ClassNotFoundException {
        List<Company> companies = Files
                .newBufferedReader(Paths.get(csvFile))
                .lines()
                .skip(1)
                .map(Company::fromCSV)
                .collect(Collectors.toList());

        Path serFile = Paths.get(String.format("./out/%s.ser", baseName(csvFile)));
        try (ObjectOutputStream out = new ObjectOutputStream(Files.newOutputStream(serFile));) {
            out.writeObject(companies);
        }

        try (ObjectInputStream in = new ObjectInputStream(Files.newInputStream(serFile))) {
            List<Company> companies2 = (List<Company>) in.readObject();
            if (companies.equals(companies2)) {
                System.out.println("Stored and Restored matches");
            }else {
                System.out.println("Stored and Restored DO NOT match");
            }
        }
    }

    String baseName(String filename) {
        Path   path   = Paths.get(filename);
        String file   = path.getFileName().toString();
        int    extPos = file.lastIndexOf('.');
        return file.substring(0, extPos);
    }
}
