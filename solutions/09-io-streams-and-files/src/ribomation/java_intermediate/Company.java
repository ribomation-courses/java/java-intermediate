package ribomation.java_intermediate;

import java.io.Serializable;
import java.util.Objects;

public class Company implements Serializable {
    private String name, email, street, city, country;

    public Company() {
    }

    public Company(String name, String email, String street, String city, String country) {
        this.name = name;
        this.email = email;
        this.street = street;
        this.city = city;
        this.country = country;
    }

    public static Company fromCSV(String csv) {
        return fromCSV(csv, ",");
    }

    public static Company fromCSV(String csv, String delim) {
        Company company = new Company();

        String[] fields = csv.split(delim);
        int      idx    = 0;
        //company,email,street,city,country
        company.name = fields[idx++];
        company.email = fields[idx++];
        company.street = fields[idx++];
        company.city = fields[idx++];
        company.country = fields[idx++];

        return company;
    }

    @Override
    public String toString() {
        return "Company{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", street='" + street + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Company company = (Company) o;
        return Objects.equals(getName(), company.getName()) &&
                Objects.equals(getEmail(), company.getEmail()) &&
                Objects.equals(getStreet(), company.getStreet()) &&
                Objects.equals(getCity(), company.getCity()) &&
                Objects.equals(getCountry(), company.getCountry());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getEmail(), getStreet(), getCity(), getCountry());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
