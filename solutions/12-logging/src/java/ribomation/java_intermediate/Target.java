package ribomation.java_intermediate;

import java.util.logging.Logger;

public class Target {
    private final Logger log;

    public Target() {
        log = Logger.getLogger(getClass().getName());
        log.info("new Target()");
    }

    public void doit(int n) {
        log.entering(getClass().getName(), "doit", n);

        log.fine("Invoke compute()");
        long result = compute(n);

        try {
            result = compute(-n);
        } catch (Exception e) {
            log.throwing(getClass().getName(), "doit", e);
        }

        log.exiting(getClass().getName(), "doit", result);
    }

    public long compute(int n) {
        log.entering(getClass().getName(), "compute", n);
        if (n < 0) {
            throw new IllegalArgumentException("Negative argument: " + n);
        }
        return n * 42;
    }
}
