package ribomation.java_intermediate;

import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class App {
    private static Logger log;

    public static void main(String[] args) {
        if (args.length > 0 && args[0].equals("-code")) {
            configureLoggingByCode();
        } else {
            System.setProperty("java.util.logging.config.file", "./src/conf/logging.properties");
        }

        log = Logger.getLogger(App.class.getName());
        log.entering(App.class.getName(), "main");
        log.info("Creating App");
        App app = new App();
        app.run();

        log.exiting(App.class.getName(), "main");
    }

    public App() {
        log.warning("new App()");
    }

    void run() {
        Target t =new Target();
        t.doit(3);
    }

    static void configureLoggingByCode() {
        Logger global = Logger.getLogger("");
        global.setLevel(Level.ALL);
        for (Handler h : global.getHandlers()) {
            System.out.println("Handler: " + h);
            h.setLevel(Level.FINER);
        }

        log.info("Configured logging by code");
    }

}
